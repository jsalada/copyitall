"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 (c) Joao Salada
"""
#!/usr/bin/python

import os
import sys
import shutil


def checkFile(fileName,fileType):
 """Checks if a 'FileName' belongs to a 'FileType' name.
 
 Returns: boolean
 """
 if not fileName.endswith(fileType):
  return False
 return True

def checkFileTypesSanity():
 """Assures filetype always starts with '.'
 
 Returns: boolean
 """
 for i in range(3,3+numFileTypes):
  if sys.argv[i][0] != '.':
   sys.stderr.write("Expected format: <.filetyte> | \
   		    Given format: <"+sys.argv[i]+">\n")


def copyFiles():
 """ Copies all files within rootdir and its subdirs which match 
 the fileTypes parametrized through argv.
 
 Keyword Arguments:
 rootdir -- root directory where search will start.
 numFileType -- number of filetypes to search.
 """
 for root, subFolders, files in os.walk(rootdir):
  for fileName in files:
   for index in range(3,3+numFileTypes):
    fileType = sys.argv[index]
    if checkFile(fileName,fileType):
     print "Copying file " + fileName + ";"
     shutil.copy(os.path.join(root, fileName),destdir)


################################
########### MAIN ###############
################################

if  __name__ == '__main__':
 numArgs = len(sys.argv)

 if numArgs < 4:
  sys.stderr.write("usage: <src-root-dir> <dest-dir> <.filetype>\#\n")
  sys.exit()

 rootdir = sys.argv[1]
 destdir = sys.argv[2]
 numFileTypes = len(sys.argv) - 3 #3= argv[0-2]

 checkFileTypesSanity()
 copyFiles()



   
